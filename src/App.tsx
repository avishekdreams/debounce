import { useEffect, useState, useMemo } from 'react';
import './App.css';
import useDebounce from './hooks/useDebounce';
import List from './List';

function App() {
  const [search, setSearch] = useState<string | null>(null);
  const deBounceSearch = useDebounce(search, 2000);
  
  useEffect(() => {
    if (deBounceSearch) {
    } else {
    }
  }, [deBounceSearch]);

  const handleClick = (id: number) => {
    alert(`Id for the selected row is ${id}`);
  }
  
  const cachedList = useMemo(() => <List term={deBounceSearch} handleClick={handleClick} />, [deBounceSearch]);

  return (
    <div className="App">
      <h2>Search Something</h2>
      <input type="search" placeholder='Enter term' onChange={(e) => setSearch(e.target.value)} />
      {deBounceSearch !== '' && cachedList}
    </div>
  );
}

export default App;

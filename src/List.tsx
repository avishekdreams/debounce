import React, { useEffect, useState } from 'react';

interface Iprops {
    term: string,
    handleClick: (id: number) => void;
}
interface Todos {
    title: string,
    id: number,
}

const fetchTodos = async (term: string) => {
    const url = `https://jsonplaceholder.typicode.com/todos?search=${term}`;
    const response = await fetch(url);
    const result = await response.json();
    return result;
}

let List: React.FC<Iprops> = ({ term, handleClick }) => {
    const [todos, setTodos] = useState<Todos[]>([]);
    useEffect(() => {
        fetchTodos(term).then(setTodos)
    }, [term]);
    return (
        <div className='todoDiv'>
            {todos.map((todo) => {
                return <li className='todoList' key={todo.id}>
                    <span>{todo.title}</span>
                    <button onClick={() => handleClick(todo.id)}>Click for Info</button>
                </li>
            })}
        </div>
    )
}

export default List;